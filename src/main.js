import App from './App.vue'
import router from './router'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Connector from "./Services/SocketConnector";
import Account from "./Services/Account";
import Requests from "./Services/Requests";
import store from './store'



Vue.use(VueRouter)
Vue.prototype.$endpointHost = process.env.VUE_APP_ENDPOINT_HOST
Vue.prototype.$publicPath = process.env.BASE_URL
Vue.prototype.$auth = new Account()
Vue.prototype.$requests =   Request 




router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requiresAuth)) {
    Vue.prototype.$auth.validate(() => {
        return next();
      },
      () => {
        localStorage.removeItem('')
        return next({
          name: 'Authorization'
        });
      }
    )
  }
  if (to.matched.some(route => route.meta.guestOnly)) {
    Vue.prototype.$auth.validate(() => {
        return next({
          name: 'Home'
        });
      },
      () => {
        return next();
      }
    )
  }
  next();

})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})