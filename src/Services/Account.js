const requests = require('./Requests')

export default class Account {
    interval_id = undefined;
    logging_in = false
    is_validating = false

    constructor(username) {
        this.username = username
    }


    login_or_register(username, password = undefined, callback = undefined) {
        this.logging_in = true;
        requests.post(`//${process.env.VUE_APP_ENDPOINT_HOST}/api/auth/login_or_register`, {
            username,
            password
        }).then((response) => {
            localStorage.setItem("token", response.data.token);
            this.logging_in = false;

            if (callback !== undefined) {
                callback()
            }
        });


    }

    set_profile_image(image, callback = undefined) {

        let fd = new FormData()

        fd.append("photo", image, "image.jpg");

        requests.post(`//${process.env.VUE_APP_ENDPOINT_HOST}/api/profile/image`, fd, {
            'Content-Type': 'multipart/form-data'
        }).then((response) => {
            if (callback !== undefined) {
                callback()
            }
        });
    }

    logout(on_success) {
        requests.post(`//${process.env.VUE_APP_ENDPOINT_HOST}/api/auth/logout`, {}).then(() => {
            localStorage.removeItem('token')
            if (on_success !== undefined) {
                on_success()
            }
        })

    }



    validate(on_success, on_failure) {
        if (this.is_validating) {
            return;
        }


        if (localStorage.getItem('token') === null) {
            on_failure()
            return;
        }
        this.is_validating = true

        const self = this
        requests.get(`//${process.env.VUE_APP_ENDPOINT_HOST}/api/auth/validate`)
            .then(() => {
                self.is_validating = false

                if (on_success !== undefined) {
                    on_success()
                }
            })
            .catch((error) => {
                self.is_validating = false
                localStorage.removeItem('token')

                if (on_failure !== undefined) {
                    on_failure()
                }

            })

    }

    get_messages(on_success) {
        requests.get(`//${process.env.VUE_APP_ENDPOINT_HOST}/communicator/connections/messages`)
            .then((response) => {
                if (on_success !== undefined) {
                    on_success(response.data)
                }
            })

    }

}

export {
    Account
}