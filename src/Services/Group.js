const requests = require('./Requests')

export default class Group {


    get_groups(callback) {
        requests.get(`//${process.env.VUE_APP_ENDPOINT_HOST}/communicator/groups/all`).then((response) => {

            if (callback !== undefined) {
                callback(response.data)
            }
        });
    }
 
}