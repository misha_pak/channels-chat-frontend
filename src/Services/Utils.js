let options = {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit"
};


export function toTime(dateString) {
    return new Date(dateString).toLocaleTimeString("en-us", options)
}

export function get_hours_minutes(raw_date) {
    let date = new Date(raw_date).toLocaleTimeString("en-us", {
        hour: "2-digit",
        minute: "2-digit",
    })

    return date;
}