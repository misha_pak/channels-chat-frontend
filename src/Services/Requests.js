const axios = require('axios')



let configuration = {
    headers: {}
}



function update_token() {
    const token = localStorage.getItem("token")

    let authorization = token === null ? null : `Token ${token}`;
    configuration.headers.Authorization = authorization
}




class Requests {

    static error_handler(error, custom_callback) {
        console.log('This error occured during request', error) // handle errors here

        if (custom_callback !== undefined) {
            custom_callback()
        }
        throw error
    }

    static async get(endpoint) {
        update_token()

        return await axios
            .get(
                endpoint,
                configuration
            )
            .catch(this.error_handler);

    }

    static async post(endpoint, data, additional_headers = {}) {
        update_token()
        return await axios
            .post(
                endpoint,
                data, {
                    ...configuration,
                    ...additional_headers
                }
            )
            .catch(this.error_handler);

    }

}

module.exports = Requests;