const {
  Account
} = require('./Account')

export default class Connector {
  on_socket_open
  scope
  id

  constructor(scope, id, on_socket_open) {
    this.on_socket_open = on_socket_open
    this.scope = scope
    this.id = id

    let q = new Account()
    q.validate(() => {
      this._create_connection()
    })
  }




  _create_connection() {
    let token = localStorage.getItem("token")

    if (token === undefined || token === null) {
      console.log('token is empty; cannot use websockets')
      return
    }

    document.cookie = `Authorization=${localStorage.getItem("token")};SameSite=Strict`;
    this.socket = new WebSocket(`ws://localhost:8000/ws/${this.scope}/${this.id}/`);

    this.socket.onopen = this.on_socket_open
    this.socket.onmessage = this.on_message
    this.socket.onclose = this.on_close

  }


  on_close(event) {
    console.error('socket disconnected')
  }

  on_message(event) {
    const data = JSON.parse(event.data);
  }

  add_on_message_listener(predicate) {
    this.socket.addEventListener('message', predicate)
  }

  send_message(message) {
    this.socket.send(JSON.stringify({
      'type': 'message',
      'message': message,
    }));

  }

  send_command(command) {
    this.socket.send(JSON.stringify({
      'command': command
    }));
  }

  request_history() {
    this.send_command('get_history')
  }
  request_last_msg() {
    this.send_command('get_last_msg')
  }

  close() {
    this.socket.close()
  }
}