import Router from 'vue-router'

const Home = () =>
    import('@/containers/Home')
const Authorization = () =>
    import('@/containers/Authorization')

const Chat = () =>
    import('@/components/Chat')

export default new Router({
    mode: 'hash', // https://router.vuejs.org/api/#mode
    linkActiveClass: 'active',
    scrollBehavior: () => ({
        y: 0
    }),
    routes: configRoutes()
})



function configRoutes() {
    return [{
        path: '/auth',
        name: 'Authorization',
        component: Authorization,
        meta: {
            guestOnly: true
        },
    }, {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            requiresAuth: true
        },
        children: [{
                path: 'chat/:scope/:id',
                name: 'chat',
                component: Chat

            },

        ]
    }]

}